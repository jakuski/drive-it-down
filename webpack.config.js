const webpack = require("webpack");
const path = require("path");

const dev = false;

module.exports = {
	mode: dev ? "development" : "production",
	target: "web",
	entry: "./src/index.jsx",
	devtool: "none",
	output: {
		path: path.resolve("./public/assets"),
		filename: "bundled-javascript.js",
	},
	resolve: {
		extensions: [".jsx", ".js"]
	},
	plugins: dev ? null : [
		new webpack.DefinePlugin({
			"process.env.NODE_ENV": JSON.stringify("production")
		})
	],
	optimization: {
		minimize: !dev
	},
	module: {
		rules: [{
			test: /\.jsx?$/,
			use: {
				loader: "babel-loader",
				options: {
					presets: [
						[
							"@babel/preset-react",
							{
								development: false,
							}
						],[
							"@babel/preset-env",
							{
								targets: "defaults, not dead"
							}
						]
					]
				}
			}
		}]
	}
};