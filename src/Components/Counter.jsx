import React from "react";
import Flex from "./Flex";
import Header from "./Header";


class CounterButton extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			interval: null,
			isHeld: false,
			computedProps: null
		};
		this.handlePress = this.handlePress.bind(this);
		this.handleRelease = this.handleRelease.bind(this);
	}
	handleRelease() {
		this.setState(s => {
			clearInterval(s.interval);
			s.isHeld = false;
			return s;
		});
	}
	handlePress(e) {
		const TickCallback = this.props.onTick;

		TickCallback(e); // fire 1 straight away.

		this.setState(s => {
			s.interval = setInterval(TickCallback, this.props.timeout,e);
			s.isHeld = true;
			return s;
		});
	}
	render() {
		return React.createElement("button", {
			onMouseDown: this.handlePress,
			onTouchStart: this.handlePress,
			onMouseUp: this.handleRelease,
			onTouchEnd: this.handleRelease,
			onMouseLeave: this.handleRelease,
			className: `counter ${this.props.show ? "" : "hide"}`,
		}, "More");
	}
}

class Counter extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			count: 0,
			reachedMax: false,
		};
		this.handleCountDown = this.handleCountDown.bind(this);
		this.handleCountUp = this.handleCountUp.bind(this);
		this.onReachMax = this.onReachMax.bind(this);
	}
	handleCountDown() {
		this.setState(s => {
			let c = s.count - this.props.incrementAmount;
			if (c < this.props.min) {
				c = this.props.min;
				if (typeof this.props.onReachMin === "function") this.props.onReachMin(c);
			}
			s.count = c;
			return s;
		});
	}
	handleCountUp() {
		this.setState(s => {
			let c = s.count + this.props.incrementAmount;
			if (c > this.props.max) {
				c = this.props.max;
				//if (typeof this.props.onReachMax === "function") this.props.onReachMax(c);
				this.onReachMax();
			}
			s.count = c;
			return s;
		});
	}
	onReachMax() {
		this.setState({reachedMax: true});
		this.props.onReachMax();
	}
	render() {
		return <div className="counter">
			<Flex preset="centre">
				<Header><span style={{color: this.state.reachedMax ? "#ff5454" : "white", fontSize: this.state.reachedMax ? "130px" : "inherit"}}>
				£{this.state.count}
				</span></Header>
				<CounterButton onTick={this.handleCountUp} timeout={this.props.timeout} show={!this.state.reachedMax}/>
			</Flex>
		</div>;
	}
}

export default Counter;