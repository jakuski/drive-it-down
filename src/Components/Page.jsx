import React from "react";
import Flex from "./Flex";
import Header from "./Header";

const Page = props => {
	return props.show ? <div>
		<Tab/>
		<div className="page">
			<Flex preset="centre">
				<Header size="2">Don't think it's right?
				<br />
				<p style={{fontWeight:"400"}}>We don't either. That's why we need to drive it down.

				<br/>
				<br/>

				<a href="https://www.amnesty.org.uk/actions/home-office-stop-profiteering-childrens-rights">Join the fight with Amnesty international today.</a>

				<br />
				<br />

				<img className="logo" src="img/logo.png" />

				</p>
				</Header>
			</Flex>

		</div>
	</div> : <React.Fragment />;
};

const Tab = () => {
	return <div class="tab">We're dissapointed too. ↓</div>;
};

export default Page;