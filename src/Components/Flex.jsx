import React from "react";

const Flex = props => {
	return React.createElement(props.elType || "div", {
		className: `flex ${props.preset || ""}`
	}, props.children);
};

export default Flex;