import React from "react";

const Header = props => {
	return React.createElement(
		`h${props.size || "1"}`,
		{className: "header"},
		props.children
	);
};

export default Header;