import React from "react";

const Background = props => {
	return <div {...props} className={`background-wrap ${props.bg}`} style={{height: "100%" || props.h}}>
		{props.children}
	</div>;
};

export default Background;