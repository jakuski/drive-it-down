import React from "react";
import Background from "./Background";
import Flex from "./Flex";
import Header from "./Header";
import Counter from "./Counter";
import Page from "./Page";

class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			counterReachedMax: false
		};

		this.onCounterReachMax = this.onCounterReachMax.bind(this);
	}
	onCounterReachMax() {
		this.setState({counterReachedMax: true});
	}
	render() {
		return <div className="app">
			<Background bg="girl-dandelion">
				<Flex preset="reverse">
					<Flex preset="down">
						<div className="guess-header">
							<Header size="1">Can you guess how much she was charged for her citizenship?</Header>
						</div>
						<Counter timeout={10} incrementAmount={3} min={0} max={1012} onReachMax={this.onCounterReachMax}/>
					</Flex>
					<div />
				</Flex>
			</Background>
			<Page show={this.state.counterReachedMax} />
		</div>;
	}
}

export default App;